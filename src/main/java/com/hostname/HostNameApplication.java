package com.hostname;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class HostNameApplication {

	public static void main(String[] args) {
		SpringApplication.run(HostNameApplication.class, args);
	}

}

@RestController("/host")
class Controller{
	
	@GetMapping("/")
	public String hostName(HttpServletRequest request) {
		return request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
	}
}
